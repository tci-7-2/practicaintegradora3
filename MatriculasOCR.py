import cv2
import pytesseract

# Ruta al ejecutable de Tesseract OCR
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'

def determinarEstado(placa):
    # Definición de los rangos de letras para cada estado
    estados = {
        "Aguascalientes": ("AAA", "AFZ"),
        "Baja California": ("AGA", "CYZ"),
        "Baja California Sur": ("CZA", "DEZ"),
        "Campeche": ("DFA", "DKZ"),
        "Chiapas": ("DLA", "DSZ"),
        "Chihuahua": ("DTA", "ETZ"),
        "Coahulia": ("EUA", "FPZ"),
        "Colima": ("FRA", "FWZ"),
        "Durango": ("FXA", "GFZ"),
        "Estado de Mexico": ("LGA", "PEZ"),
        "Guanajuato": ("GGA", "GYZ"),
        "Guerrero": ("GZA", "HFZ"),
        "Hidalgo": ("HGA", "HRZ"),
        "Sinaloa": ("HSA", "LFZ"),
        "Jalisco": ("PFA", "PUZ"),
        "Michoacan": ("PVA", "RDZ"),
        "Morelos": ("REA", "RJZ"),
        "Nayarit": ("RKA", "TGZ"),
        "Nuevo Leon": ("THA", "TMZ"),
        "OAXACA": ("TNA", "UJZ"),
        "Puebla": ("UKA", "UPZ"),
        "Queretaro": ("URA", "UVZ"),
        "Quintana Roo": ("UWA", "VEZ"),
        "San Luis Potosi": ("VFA", "VSZ"),
        "Sonora": ("VTA", "WKZ"),
        "Tabasco": ("WLA", "WWZ"),
        "Tamaulipas": ("WXA", "XSZ"),
        "Tlaxcala": ("XTA", "XXZ"),
        "Veracruz": ("XYA", "YVZ"),
        "Yucatan": ("YWA", "ZCZ"),
        "Zacatecas": ("ZDA", "ZHZ")
    }

    placa = placa.upper()  # Convertir a mayúsculas para hacer las comparaciones case-insensitive

    # Iterar sobre los estados y sus rangos de letras
    for estado, (inicio, fin) in estados.items():
        # Verificar si la placa está dentro de algún rango de letras de un estado
        if inicio <= placa <= fin:
            return estado  # Retorna el estado correspondiente

    return "Estado no válido"  # Si no coincide con ningún rango, retorna un mensaje de estado inválido

placa = []

# Cargar la imagen
image = cv2.imread(r'C:\Users\ultim\Desktop\PracticaIntegradora3\auto001.jpg')

# Convertir la imagen a escala de grises
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Aplicar un filtro de desenfoque para reducir el ruido
gray = cv2.blur(gray, (3, 3))

# Detectar los bordes en la imagen
canny = cv2.Canny(gray, 150, 200)

# Dilatar los bordes para cerrar cualquier brecha
canny = cv2.dilate(canny, None, iterations=1)

# Encontrar los contornos en la imagen
cnts, _ = cv2.findContours(canny, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

# Iterar sobre los contornos encontrados
for c in cnts:
    area = cv2.contourArea(c)

    x, y, w, h = cv2.boundingRect(c)
    epsilon = 0.09 * cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, epsilon, True)

    # Verificar si el contorno es un rectángulo y tiene un área mínima
    if len(approx) == 4 and area > 9000:
        print('area=', area)
        cv2.drawContours(image, [c], 0, (0, 255, 0), 3)

        aspect_ratio = float(w) / h
        # Verificar si el aspect ratio del contorno es mayor que 0.5
        if aspect_ratio > 0.5:
            # Recortar la región de la placa de la imagen original
            placa = gray[y:y + h, x:x + w]
            
            # Utilizar Tesseract OCR para reconocer el texto en la placa
            text = pytesseract.image_to_string(placa, config='--psm 13')
            print('PLACA: ', text)
            
            # Determinar a qué estado pertenece la placa
            estado = determinarEstado(text)
            print('Estado:', estado)
            
            # Mostrar la región de la placa y dibujar un rectángulo alrededor de ella en la imagen original
            cv2.imshow('PLACA', placa)
            cv2.moveWindow('PLACA', 780, 10)
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 3)

# Mostrar la imagen original con los contornos y la región de la placa identificada
cv2.imshow('Image', image)
cv2.moveWindow('Image', 45, 10)
cv2.waitKey(0)
